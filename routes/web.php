<?php

use App\Http\Controllers\ArticleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/article', [ArticleController::class,'index'])->name('article.index');
Route::get('/article/create', [ArticleController::class,'create'])->name('article.create');
Route::post('/article',[ArticleController::class,'store'])->name('article.store');
Route::put('/article',[ArticleController::class,'update'])->name('article.update');
Route::delete('/article/{article}',[ArticleController::class,'destroy'])->name('article.destroy');
Route::get('/article/edit/{article}', [ArticleController::class,'edit'])->name('article.edit');
