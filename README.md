# Clone Respository
`git clone https://gitlab.com/shevaathalla/crud-laravel-inertia.git`

# Go to directory
`cd crud-laravel-inertia`

# Install laravel
`composer install`

# Install dependencies
`npm install`
# Copy .env
`cp .env.example .env`
- Then create database and edit database in .env

# Generate Key
`php artisan key:generate`

# Run Laravel 
`php artisan serve`