<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class ArticleController extends Controller
{
    public function index()
    {

        return Inertia::render('Article/Index', ['articles' => Article::all()]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'text' => 'required',
        ]);
        Article::create([
            'title' => $request->title,
            'text' => $request->text,
        ]);

        return Redirect::route('article.index');
    }
    public function create()
    {
        return Inertia::render('Article/Create');
    }
    public function edit(Article $article)
    {
        return Inertia::render('Article/Edit', [
            'article' => $article
        ]);
    }

    public function update(Request $request)
    {        
        $request->validate([
            'title' => ['required'],
            'text' => ['required'],
        ]);        
        Article::find($request->id)->update([
            'title' => $request->title,
            'text' => $request->text,
        ]);


        return Redirect::route('article.index');
    }

    public function destroy(Article $article){
        Article::destroy($article->id);
        return Redirect::route('article.index');
    }
}
